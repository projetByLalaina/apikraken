# Application Kraken
OBJECTIF: 
## Mise en niveau Symfony 5.
## Création complète d'une API kraken.
## Interface graphique création de kraken basée sur Kraken API.

# Environnement de développement
* PHP 7.4
* Composer
* Symfony CLI

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la Cli Symfony):

```bash
symfony check requirements
```

```bash
symfony composer req debug --dev
```

### lancer l'environnement de développement
```bash
symfony serve -d
```

## Base de données
* Kraken : name, age, height, weight
* Power: name, max_use  (liste power : blast, plague, mind control, ink fog, force shield, regeneration)
* Tentacule: name, health_point, strength, dexterity, constitution


## Commande
```bash
php bin/console d:d:c
symfony console m:e
symfony console m:migr
symfony console d:m:m
```

## Create fixture
```bash
composer require fzaninotto/faker
composer require orm-fixtures --dev
php bin/console m:f nom_fixture
php bin/console d:f:l non_fixture
```

 


