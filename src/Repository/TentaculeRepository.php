<?php

namespace App\Repository;

use App\Entity\Tentacule;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


/**
 * @method Tentacule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tentacule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tentacule[]    findAll()
 * @method Tentacule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TentaculeRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tentacule::class);
    }

    /**
     * @return Tentacule[] Returns an array of Tentacule objects
     */
    public function deleteTentacule(Tentacule $value)
    {
        var_dump('value', $value); 
        $entityManager = $this->getEntityManager();
        dd('repository');

        $entityManager->remove($value);
        $entityManager->flush();

        dd('repository2');
    }

    /**
     * Supprimer tentacule sur le kraken
     */
    public function supprimerTentacule(Tentacule $tentacule){
        $entityManager = $this->getEntityManager();
        $entityManager->remove($tentacule);
        $entityManager->flush();
    }

    /**
     * Verify Tentacule
     */
    public function verifyTentacule(int $valueId){
        return null;
    }


    /**
     * compte nombre tentacule
     */
    public function compteNbTentacule($idKraken){
        return $this->createQueryBuilder('t')
        ->select('count(t.id)')
        ->andWhere('t.kraken = :val')
        ->setParameter('val', $idKraken)
        ->getQuery()
        ->getOneOrNullResult();
    }

    /**
     * verify name power in tentacule
     */
    public function compteNbTentaculeParPower($idPower, $idKraken){
        return $this->createQueryBuilder('t')
        ->select('count(t.id)')
        ->andWhere('t.power = :valPower')
        ->andWhere('t.kraken = :valKraken')
        ->setParameter('valPower', $idPower)
        ->setParameter('valKraken', $idKraken)
        ->getQuery()
        ->getOneOrNullResult();
    }

}
