<?php

namespace App\Repository;

use App\Entity\Kraken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Kraken|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kraken|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kraken[]    findAll()
 * @method Kraken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KrakenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kraken::class);
    }

    /**
     * Retourner une liste de Kraken
     * 
     * @return array
     */
    public function listKraken(): array{
        $queryBuilder = $this->createQueryBuilder('k')
                            ->select('k.name', 'k.age', 'k.height', 'k.weight')
                            ->orderBy('k.name', 'DESC');

        $query = $queryBuilder->getQuery();
        return $query->execute();

    }

    /**
     * creation power
     */
    public function createKraken($entityManager, $name, $age, $height, $weight){
        $kraken = new Kraken();
        $kraken->setName($name);
        $kraken->setAge($age);
        $kraken->setHeight($height);
        $kraken->setWeight($weight);
        $entityManager->persist($kraken);
        $entityManager->flush();
        return $kraken;
    }

}

