<?php

namespace App\Form;

use App\Entity\Tentacule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TentaculeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('health_point')
            ->add('strength')
            ->add('dexterity')
            ->add('constitution')
            ->add('power')
            ->add('kraken')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tentacule::class,
        ]);
    }
}
