<?php

namespace App\Controller;

use App\Entity\Tentacule;
use App\Entity\Kraken;
use App\Entity\Power;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\Common\Persistence\ObjectManager;

use App\Repository\KrakenRepository;
use App\Repository\TentaculeRepository;
use App\Repository\PowerRepository;
use Symfony\Component\Serializer\SerializerInterface;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\JsonResponse;

class TentaculeController extends AbstractController
{

    /**
     * @var ObjectManager
     */
    private $em;

    private $tentacule;

    public function _construct(ObjectManager $em, Tentacule $tentacule){
        $this->tentacule = $tentacule;
        $this->em = $em;
    }

    /**
     * @Route("/tentacule", name="tentacule")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TentaculeController.php',
        ]);
    }

    /**
     * @Route("/listTentacule", name="list_tentacule", methods={"GET"})
     */
    public function listTentacule(TentaculeRepository $tentaculeRepository){
        return $this->json($tentaculeRepository->findAll(), 200, [], ['groups'=> 'post:read']);
    }

    /**
     * @Route("/saveOrDelete", name="save_Or_Delete", methods={"POST|GET"})
     */
    public function saveOrDelete(Request $request, EntityManagerInterface $em, 
                                    TentaculeRepository $tentaculeRepository,
                                    KrakenRepository $krakenRepository,
                                    PowerRepository $powerRepository,
                                    SerializerInterface $serializer
                                    ){
        $jsonRecu = json_decode($request->getContent());
        if(empty($tentaculeRepository->find($jsonRecu->id))){
            if (empty($krakenRepository->find($jsonRecu->kraken))) {
                $kraken = $krakenRepository->createKraken($em, $jsonRecu->name, 5, 135, 158);
                $tentacule = $serializer->deserialize($request->getContent(), Tentacule::class, 'json');
                $tentacule->setKraken($krakenRepository->find($kraken->getId()));
                if(empty($powerRepository->find($jsonRecu->power))){
                    $power = $powerRepository->createPower($em, $krakenRepository->find($kraken->getId()), $jsonRecu->name,5);
                    $tentacule->setPower($powerRepository->find($power->getId()));
                }else{
                    $tentacule->setPower($powerRepository->find($jsonRecu->power));
                }
                $em->persist($tentacule);
                $em->flush();
                return $this->json(["message" => "Tentacule crée après création kraken"]);
            }else if(empty($powerRepository->find($jsonRecu->power))){
                $powerRepository->createPower($em, $krakenRepository->find($jsonRecu->kraken), $jsonRecu->name,5);
                return $this->json(["message" => "Tentacule crée après création power"]);
            }else{
                if($tentaculeRepository->compteNbTentacule($jsonRecu->kraken)[1] == 8){
                    return $this->json(["message" => "tentacule déjà rempli"]);
                }else{
                    if((($tentaculeRepository->compteNbTentaculeParPower($jsonRecu->power, $jsonRecu->kraken))[1]) == 4){
                        return $this->json(["message" => "power déjà rempli"]);
                    }else{
                        $tentacule = $serializer->deserialize($request->getContent(), Tentacule::class, 'json');
                        $tentacule->setKraken($krakenRepository->find($jsonRecu->kraken));
                        $tentacule->setPower($powerRepository->find($jsonRecu->power));
                        $em->persist($tentacule);
                        $em->flush();
                        return $this->json(["message" => "Ajout tentacule réussie"]);
                    }
                    return $this->json(["message" => "tentacule déjà rempli"]);
                }
            }
        }else{
            $tentaculeRepository->supprimerTentacule($tentaculeRepository->find($jsonRecu->id));
            return $this->json(["message" => "Suppression tentacule"]);
        }
    }

    /**
     * @Route("/delete/{id}", name="delete_tentacule", methods={"GET"})
     */
    public function deleteTentacule($id, TentaculeRepository $tentaculeRepository){
        return $this->json($tentaculeRepository->supprimerTentacule($tentaculeRepository->find($id)), 200);
    }

}
