<?php

namespace App\Entity;

use App\Repository\PowerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PowerRepository::class)
 */
class Power
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("post:read")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups("post:read")
     */
    private $max_use;

    /**
     * @ORM\OneToMany(targetEntity=Tentacule::class, mappedBy="power")
     */
    private $tentacules;

    /**
     * @ORM\ManyToOne(targetEntity=Kraken::class, inversedBy="powers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kraken;

    public function __construct()
    {
        $this->tentacules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaxUse(): ?string
    {
        return $this->max_use;
    }

    public function setMaxUse(string $max_use): self
    {
        $this->max_use = $max_use;

        return $this;
    }

    /**
     * @return Collection|Tentacule[]
     */
    public function getTentacules(): Collection
    {
        return $this->tentacules;
    }

    public function addTentacule(Tentacule $tentacule): self
    {
        if (!$this->tentacules->contains($tentacule)) {
            $this->tentacules[] = $tentacule;
            $tentacule->setPower($this);
        }

        return $this;
    }

    public function removeTentacule(Tentacule $tentacule): self
    {
        if ($this->tentacules->removeElement($tentacule)) {
            // set the owning side to null (unless already changed)
            if ($tentacule->getPower() === $this) {
                $tentacule->setPower(null);
            }
        }

        return $this;
    }

    public function getKraken(): ?Kraken
    {
        return $this->kraken;
    }

    public function setKraken(?Kraken $kraken): self
    {
        $this->kraken = $kraken;

        return $this;
    }
}
