<?php

namespace App\Entity;

use App\Repository\KrakenRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=KrakenRepository::class)
 */
class Kraken
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("post:read")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $age;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     * @Groups("post:read")
     */
    private $height;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     * @Groups("post:read")
     */
    private $weight;

    /**
     * @ORM\OneToMany(targetEntity=Power::class, mappedBy="kraken")
     */
    private $powers;

    /**
     * @ORM\OneToMany(targetEntity=Tentacule::class, mappedBy="kraken")
     */
    private $tentacules;

    public function __construct()
    {
        $this->tentacules = new ArrayCollection();
        $this->powers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return Collection|Power[]
     */
    public function getPowers(): Collection
    {
        return $this->powers;
    }

    public function addPower(Power $power): self
    {
        if (!$this->powers->contains($power)) {
            $this->powers[] = $power;
            $power->setKraken($this);
        }

        return $this;
    }

    public function removePower(Power $power): self
    {
        if ($this->powers->removeElement($power)) {
            // set the owning side to null (unless already changed)
            if ($power->getKraken() === $this) {
                $power->setKraken(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tentacule[]
     */
    public function getTentacules(): Collection
    {
        return $this->tentacules;
    }

    public function addTentacule(Tentacule $tentacule): self
    {
        if (!$this->tentacules->contains($tentacule)) {
            $this->tentacules[] = $tentacule;
            $tentacule->setKraken($this);
        }

        return $this;
    }

    public function removeTentacule(Tentacule $tentacule): self
    {
        if ($this->tentacules->removeElement($tentacule)) {
            // set the owning side to null (unless already changed)
            if ($tentacule->getKraken() === $this) {
                $tentacule->setKraken(null);
            }
        }

        return $this;
    }
}
