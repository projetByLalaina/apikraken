<?php

namespace App\Entity;

use App\Repository\TentaculeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TentaculeRepository::class)
 */
class Tentacule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("post:read")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("post:read")
     */
    private $health_point;

    /**
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $strength;

    /**
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $dexterity;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("post:read")
     */
    private $constitution;

    /**
     * @ORM\ManyToOne(targetEntity=Power::class, inversedBy="tentacules")
     * @ORM\JoinColumn(name="power_id", referencedColumnName="id")
     * @Groups("post:read")
     */
    private $power;

    /**
     * @ORM\ManyToOne(targetEntity=Kraken::class, inversedBy="tentacules")
     * @ORM\JoinColumn(name="kraken_id", referencedColumnName="id")
     * @Groups("post:read")
     */
    private $kraken;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHealthPoint(): ?string
    {
        return $this->health_point;
    }

    public function setHealthPoint(string $health_point): self
    {
        $this->health_point = $health_point;

        return $this;
    }

    public function getStrength(): ?int
    {
        return $this->strength;
    }

    public function setStrength(int $strength): self
    {
        $this->strength = $strength;

        return $this;
    }

    public function getDexterity(): ?int
    {
        return $this->dexterity;
    }

    public function setDexterity(int $dexterity): self
    {
        $this->dexterity = $dexterity;

        return $this;
    }

    public function getConstitution(): ?string
    {
        return $this->constitution;
    }

    public function setConstitution(string $constitution): self
    {
        $this->constitution = $constitution;

        return $this;
    }

    public function getPower(): ?Power
    {
        return $this->power;
    }

    public function setPower(?Power $power): self
    {
        $this->power = $power;

        return $this;
    }

    public function getKraken(): ?Kraken
    {
        return $this->kraken;
    }

    public function setKraken(?Kraken $kraken): self
    {
        $this->kraken = $kraken;

        return $this;
    }
}
