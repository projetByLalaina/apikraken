<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use App\Entity\Kraken;
use App\Entity\Power;

class PowerFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        
        for($j = 0; $j < 10; $j++){
            $kraken = new Kraken();
            $kraken->setName($faker->name);
            $kraken->setAge($j++);
            $kraken->setHeight($faker->numberBetween($min = 120, $max = 180));
            $kraken->setWeight($faker->numberBetween($min = 120, $max = 180));
            $manager->persist($kraken);
            for($i = 0; $i < 2; $i++){
                $power = new Power();
                $power->setName($faker->name);
                $power->setMaxUse($faker->numberBetween($min = 1, $max = 10));
                $power->setKraken($kraken);
                $manager->persist($power);
                $i++;
            }
            $j++;
        }
        $manager->flush();
    }
}
