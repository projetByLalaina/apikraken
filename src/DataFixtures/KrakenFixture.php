<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use App\Entity\Kraken;

class KrakenFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 50 ; $i++){
            $kraken = new Kraken();
            $kraken->setName($faker->name);
            $kraken->setAge($i++);
            $kraken->setHeight($faker->numberBetween($min = 120, $max = 180));
            $kraken->setWeight($faker->numberBetween($min = 120, $max = 180));
            $manager->persist($kraken);
        }
        $manager->flush();
    }
}
