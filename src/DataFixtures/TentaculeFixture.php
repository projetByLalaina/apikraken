<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Faker;
use App\Entity\Kraken;
use App\Entity\Power;
use App\Entity\Tentacule;

class TentaculeFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i<25; $i++) {
            $kraken = new Kraken();
            $kraken->setName($faker->name);
            $kraken->setAge($i++);
            $kraken->setHeight($faker->numberBetween($min = 120, $max = 180));
            $kraken->setWeight($faker->numberBetween($min = 120, $max = 180));
            $manager->persist($kraken);
            for ($j = 0; $j<2; $j++){
                $power = new Power();
                $power->setName($faker->name);
                $power->setMaxUse($faker->numberBetween($min = 1, $max = 10));
                $power->setMaxUse($faker->numberBetween($min = 1, $max = 10));
                $power->setKraken($kraken);
                $manager->persist($power);

                for ($k = 0; $k<8; $k++){
                    $tentacule = new Tentacule();
                    $tentacule->setName($faker->name);
                    $tentacule->setHealthPoint($faker->numberBetween($min = 1, $max = 10));
                    $tentacule->setStrength($faker->numberBetween($min = 1, $max = 10));
                    $tentacule->setDexterity($faker->numberBetween($min = 1, $max = 10));
                    $tentacule->setConstitution($faker->name);
                    $tentacule->setPower($power);
                    $tentacule->setKraken($kraken);
                    $manager->persist($tentacule);
                    $k++;
                }
                $j++;
            }
            $i++;
        }

        $manager->flush();
    }
}
