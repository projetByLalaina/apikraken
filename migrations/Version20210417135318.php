<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210417135318 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE kraken (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, age INT NOT NULL, height NUMERIC(10, 0) NOT NULL, weight NUMERIC(10, 0) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE power (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, max_use VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tentacule (id INT AUTO_INCREMENT NOT NULL, tentacule_id INT NOT NULL, power_id INT NOT NULL, name VARCHAR(50) NOT NULL, health_point VARCHAR(50) NOT NULL, strength INT NOT NULL, dexterity INT NOT NULL, constitution VARCHAR(20) NOT NULL, INDEX IDX_B31C4D102BB10BF4 (tentacule_id), INDEX IDX_B31C4D10AB4FC384 (power_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tentacule ADD CONSTRAINT FK_B31C4D102BB10BF4 FOREIGN KEY (tentacule_id) REFERENCES kraken (id)');
        $this->addSql('ALTER TABLE tentacule ADD CONSTRAINT FK_B31C4D10AB4FC384 FOREIGN KEY (power_id) REFERENCES power (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tentacule DROP FOREIGN KEY FK_B31C4D102BB10BF4');
        $this->addSql('ALTER TABLE tentacule DROP FOREIGN KEY FK_B31C4D10AB4FC384');
        $this->addSql('DROP TABLE kraken');
        $this->addSql('DROP TABLE power');
        $this->addSql('DROP TABLE tentacule');
    }
}
