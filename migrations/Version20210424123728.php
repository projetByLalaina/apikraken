<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210424123728 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tentacule DROP FOREIGN KEY FK_B31C4D102BB10BF4');
        $this->addSql('DROP INDEX IDX_B31C4D102BB10BF4 ON tentacule');
        $this->addSql('ALTER TABLE tentacule ADD kraken_id INT DEFAULT NULL, DROP tentacule_id');
        $this->addSql('ALTER TABLE tentacule ADD CONSTRAINT FK_B31C4D108A9341DD FOREIGN KEY (kraken_id) REFERENCES kraken (id)');
        $this->addSql('CREATE INDEX IDX_B31C4D108A9341DD ON tentacule (kraken_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tentacule DROP FOREIGN KEY FK_B31C4D108A9341DD');
        $this->addSql('DROP INDEX IDX_B31C4D108A9341DD ON tentacule');
        $this->addSql('ALTER TABLE tentacule ADD tentacule_id INT NOT NULL, DROP kraken_id');
        $this->addSql('ALTER TABLE tentacule ADD CONSTRAINT FK_B31C4D102BB10BF4 FOREIGN KEY (tentacule_id) REFERENCES kraken (id)');
        $this->addSql('CREATE INDEX IDX_B31C4D102BB10BF4 ON tentacule (tentacule_id)');
    }
}
