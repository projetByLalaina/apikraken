<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210417135817 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE power ADD kraken_id INT NOT NULL');
        $this->addSql('ALTER TABLE power ADD CONSTRAINT FK_AB8A01A08A9341DD FOREIGN KEY (kraken_id) REFERENCES kraken (id)');
        $this->addSql('CREATE INDEX IDX_AB8A01A08A9341DD ON power (kraken_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE power DROP FOREIGN KEY FK_AB8A01A08A9341DD');
        $this->addSql('DROP INDEX IDX_AB8A01A08A9341DD ON power');
        $this->addSql('ALTER TABLE power DROP kraken_id');
    }
}
